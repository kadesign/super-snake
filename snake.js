const pg = document.getElementById('pg-canvas');
const scoreEl = document.getElementById('score');
let ctx;

// Field configuration
const fieldProps = {
  size: {
    w: 600,
    h: 450,
  },
  grid: {
    ptSize: 15,
    xCount: 0,
    yCount: 0,
  },
};

// Runtime properties
let curDirection = 'e';
let lastDirection = 'e';
let snakePoints = [];
let foodPosition = [];
let playerPoints = 0;
let snakeHead;
let isGameRunning = false;

// Helper functions
/**
 * Converts coordinate unit to pixels
 * @param coord Value in coordinate units
 * @returns {number} Value in pixels
 */
const coord2Px = (coord) => coord * fieldProps.grid.ptSize;

// https://stackoverflow.com/q/24943200
/**
 * Checks if array with length of 2 is a child of another array
 * @param parent Parent array
 * @param child Child array
 * @returns {boolean} Relation of arrays is confirmed
 */
const isChildArray = (parent, child) => {
  for (let i = 0; i < parent.length; i++) {
    if (parent[i][0] === child[0] && parent[i][1] === child[1]) {
      return true;
    }
  }
  return false;
};

/**
 * Calculates a random number between two provided values (including them)
 * @param a Left boundary of a range
 * @param b Right boundary of a range
 * @returns {number} Random number in range
 */
const getRandomBetween = (a, b) => {
  return a + Math.floor(Math.random() * (b - a + 1));
};

// Checking functions
/**
 * Checks if a point on axis is in the boundaries of the canvas
 * @param axis Axis
 * @param value Value in coordinate units (not pixels)
 * @returns {null|boolean} Point is in allowed range (null if axis is invalid - only 'x' and 'y' allowed)
 */
const checkCoord = (axis, value) => {
  switch (axis) {
    case 'x':
      return value < fieldProps.grid.xCount && value >= 0 && (value + 1) * fieldProps.grid.ptSize <= fieldProps.size.w;
    case 'y':
      return value < fieldProps.grid.yCount && value >= 0 && (value + 1) * fieldProps.grid.ptSize <= fieldProps.size.h;
    default:
      return null;
  }
};

/**
 * Checks if turn to desired direction is possible from current position
 * @param currentDirection Current movement direction
 * @param desiredDirection Desired movement direction
 * @returns {null|boolean} Turn is possible (null if input parameters are invalid)
 */
const checkTurn = (currentDirection, desiredDirection) => {
  // Only turns allowed. "Turn" to the same direction has no sense. Moving backwards is impossible
  switch (currentDirection) {
    case 'n':
    case 's':
      return desiredDirection === 'w' || desiredDirection === 'e';
    case 'w':
    case 'e':
      return desiredDirection === 'n' || desiredDirection === 's';
    default:
      return null;
  }
};

/**
 * Checks if a point already exists on canvas
 * @param x Coordinate of the point on x-axis
 * @param y Coordinate of the point on y-axis
 * @returns {boolean} Point exists
 */
const checkPointExist = (x, y) => {
  if (!checkCoord('x', x) || !checkCoord('y', y)) return false;

  return ctx.getImageData(coord2Px(x), coord2Px(y), fieldProps.grid.ptSize, fieldProps.grid.ptSize)
            .data.some(channel => channel !== 0);
};

/**
 * Checks if a head will collide the wall
 * @param x Coordinate of the head on x-axis
 * @param y Coordinate of the head on y-axis
 * @param nextDirection
 * @returns {null|boolean} Snake collides something (null if input data is invalid)
 */
const checkCollision = (x, y, nextDirection) => {
  // Validate input data - coordinates and direction
  if (['n', 's', 'w', 'e'].indexOf(nextDirection) === -1 || !checkCoord('x', x) || !checkCoord('y', y))
    return null;

  // Calculate next coordinates of a head and coordinates of closest wall according to movement direction
  let nextX = x,
    nextY = y,
    closestWallX,
    closestWallY;

  switch (nextDirection) {
    case 'n':
      nextY -= 1;
      closestWallX = x;
      closestWallY = -1; // temporary solution, maybe there is a better way
      break;
    case 's':
      nextY += 1;
      closestWallX = x;
      closestWallY = fieldProps.grid.yCount;
      break;
    case 'w':
      nextX -= 1;
      closestWallX = -1; // temporary solution, maybe there is a better way
      closestWallY = y;
      break;
    case 'e':
      nextX += 1;
      closestWallX = fieldProps.grid.xCount;
      closestWallY = y;
      break;
  }

  // Check collision:
  // 1. Snake hits the wall
  if ((nextX === closestWallX || nextX < 0) && (nextY === closestWallY || nextY < 0)) return true;

  // 2. Snake hits itself
  return isChildArray(snakePoints, [nextX, nextY]);
};

// Drawing (only!) methods
/**
 * Draws a point on canvas
 * @param x Coordinate of the point on x-axis
 * @param y Coordinate of the point on y-axis
 * @param type
 * @returns {null|boolean} Point was overwritten (null if input parameter values are invalid)
 */
const drawPoint = (x, y, type = 'snake') => {
  // Validate coordinates of the point
  if (!checkCoord('x', x) || !checkCoord('y', y)) return null;

  let response = false;
  const pt = new Path2D();
  pt.rect(coord2Px(x), coord2Px(y), fieldProps.grid.ptSize, fieldProps.grid.ptSize);

  // Colorize the point according to its type
  switch (type) {
    case 'snake':
      ctx.fillStyle = '#999999';
      break;
    case 'food':
      ctx.fillStyle = '#33cc33';
      break;
    case 'dead':
      ctx.fillStyle = '#ff0000';
      break;
    default:
      return null;
  }

  // Return true if the point was overwritten
  if (checkPointExist(x, y)) response = true;

  // Draw the point
  ctx.fill(pt);
  return response;
};

/**
 * Removes a point from canvas
 * @param x Coordinate of the point on x-axis
 * @param y Coordinate of the point on y-axis
 * @returns {boolean} Point is cleared
 */
const clearPoint = (x, y) => {
  // Validate coordinates of the point
  if (!checkCoord('x', x) || !checkCoord('y', y)) return false;

  // Clear the point if it exists
  if (checkPointExist(x, y)) {
    ctx.clearRect(coord2Px(x), coord2Px(y), fieldProps.grid.ptSize, fieldProps.grid.ptSize);
    return true;
  } else {
    return false;
  }
};

/**
 * Clears the whole field
 */
const clearField = () => {
  ctx.clearRect(0, 0, fieldProps.size.w, fieldProps.size.h);
};

// UI
/**
 * Updates the score on the UI
 */
const updateScore = () => {
  scoreEl.innerText = playerPoints;
};

/**
 * Reflects 'Game over' state to the UI
 */
const gameOver = () => {
  const gameOverDiv = document.createElement('div');
  gameOverDiv.className = 'suffer-bitch';
  gameOverDiv.style.width = (fieldProps.size.w + 2) + 'px';
  gameOverDiv.style.height = (fieldProps.size.h + 2) + 'px';
  gameOverDiv.appendChild(document.createElement('span'));
  gameOverDiv.children[0].innerText = 'You lose :(';

  snakePoints.forEach(point => {
    drawPoint(point[0], point[1], 'dead');
  });

  pg.parentNode.insertBefore(gameOverDiv, pg.nextSibling);
};

// Gameplay
/**
 * Calculates next position of the piece of food
 * @returns {[number, number]} New coordinates of a piece of food
 */
const getNextFoodCoords = () => {
  let foodX = getRandomBetween(0, fieldProps.grid.xCount - 1);
  let foodY = getRandomBetween(0, fieldProps.grid.yCount - 1);

  if ((foodPosition[0] === foodX && foodPosition[1] === foodY) || isChildArray(snakePoints,[foodX, foodY]))
    return getNextFoodCoords();

  return [foodX, foodY];
};

/**
 * Draws a piece of food on the field
 */
const drawFood = () => {
  foodPosition = getNextFoodCoords();
  drawPoint(foodPosition[0], foodPosition[1], 'food');
};

/**
 * Moves a snake or stops it if it hits the wall or itself
 * @returns {boolean} Movement is done
 */
const move = () => {
  // Fix movement direction
  const moveDirection = curDirection;

  // Stop movement if collision occurs
  if (checkCollision(snakeHead[0], snakeHead[1], moveDirection)) {
    gameOver();
    return false;
  }

  // Calculate next position of a head
  let nextX = snakeHead[0],
    nextY = snakeHead[1];

  switch (moveDirection) {
    case 'n':
      nextY -= 1;
      break;
    case 's':
      nextY += 1;
      break;
    case 'w':
      nextX -= 1;
      break;
    case 'e':
      nextX += 1;
      break;
  }

  // Draw a movement of the head
  snakePoints = snakePoints.concat([[nextX, nextY]]);
  drawPoint(nextX, nextY);
  snakeHead = snakePoints[snakePoints.length - 1];

  // Check if food is to be consumed
  if (foodPosition[0] === nextX && foodPosition[1] === nextY) {
    drawFood();

    // Increase player points and update UI
    playerPoints += 1;
    updateScore();
  } else {
    // Move tail forward (if food wasn't eaten)
    const tail = snakePoints[0];
    clearPoint(tail[0], tail[1]);
    snakePoints = snakePoints.slice(1);
  }

  // Fix last movement direction
  lastDirection = moveDirection;
  return true;
};

/**
 * Starts a new game
 */
const startGame = () => {
  // If game is not running, start new one
  if (!isGameRunning) {
    // Reset the game
    snakePoints.length = 0;
    foodPosition.length = 0;
    playerPoints = 0;
    curDirection = 'e';
    lastDirection = 'e';
    clearField();

    const gameOverSign = document.querySelector('.suffer-bitch');
    if (gameOverSign) {
      gameOverSign.parentNode.removeChild(gameOverSign);
    }

    // Create initial snake with length of 3
    const startX = 1,
      startY = Math.ceil(fieldProps.grid.yCount / 2) - 1;

    for (let i = 0; i + startX < 3 + startX; i++) {
      snakePoints = snakePoints.concat([[i + startX, startY]]);
      drawPoint(i + startX, startY);
    }
    snakeHead = snakePoints[2];

    // Put a piece of food on the field
    drawFood();

    // Initialize score field on the UI
    updateScore();

    // Start movement loop
    let canMove = true;
    const run = setInterval(function() {
      canMove = move();
      if (!canMove) {
        clearInterval(run);
        isGameRunning = false;
      }
    }, 250);

    isGameRunning = true;
  }
};

// Event handling
/**
 * Handles pressing keys
 * @param pressEvent 'keydown' event
 */
const handleKeyPress = (pressEvent) => {
  const keyCode = pressEvent.code;
  const usedKeys = {
    movement: [
      'ArrowLeft',
      'ArrowRight',
      'ArrowUp',
      'ArrowDown',
      'KeyW',
      'KeyA',
      'KeyS',
      'KeyD'
    ],
    reset: 'KeyR',
  };

  if (usedKeys.movement.indexOf(keyCode) !== -1) {
    pressEvent.preventDefault();
    changeDirection(keyCode);
  } else if (keyCode === usedKeys.reset) {
    pressEvent.preventDefault();
    startGame();
  }
};

/**
 * Changes movement direction according to pressed key
 * @param keyCode Key code
 */
const changeDirection = (keyCode) => {
  let desired;
  switch (keyCode) {
    case 'ArrowLeft':
    case 'KeyA':
      desired = 'w';
      break;
    case 'ArrowRight':
    case 'KeyD':
      desired = 'e';
      break;
    case 'ArrowUp':
    case 'KeyW':
      desired = 'n';
      break;
    case 'ArrowDown':
    case 'KeyS':
      desired = 's';
      break;
  }
  if (checkTurn(lastDirection, desired)) curDirection = desired;
};

// ------------------------

/**
 * Initializes the app
 */
const init = () => {
  // Calculate field size in coord units
  fieldProps.grid.xCount = fieldProps.size.w / fieldProps.grid.ptSize;
  fieldProps.grid.yCount = fieldProps.size.h / fieldProps.grid.ptSize;
  if (!Number.isInteger(fieldProps.grid.xCount) || !Number.isInteger(fieldProps.grid.yCount))
    throw new Error('Field dimensions and grid size must divide evenly');

  // If the browser supports canvas API, initialize app
  if (pg.getContext) {
    console.log('Here we go');
    ctx = pg.getContext('2d');

    // Register listener of 'keydown' events for changing movement direction
    document.addEventListener('keydown', handleKeyPress);
  }
};

/**
 * Main loop
 */
const main = () => {
  // Initialize game
  init();

  // Here goes input polling, movement processing, etc.
  startGame();
};

try {
  main();
} catch (e) {
  console.error(e);
}
